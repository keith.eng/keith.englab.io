import FoodpandaOne from '../assets/foodpanda-1.png';
import NFTInternOne from '../assets/nftintern-1.png';
import BitmexOne from '../assets/bitmex-1.png';
import EquifyteOne from '../assets/equifyte-1.png';
import KhanhvuOne from '../assets/khanhvu-1.png';

export const ProjectList = [
    {
        imgSrc: FoodpandaOne,
        client: 'foodpanda',
        tech: 'ts, node',
        link: 'https://www.foodpanda.sg',
    },
    {
        imgSrc: EquifyteOne,
        client: 'Equifyte',
        tech: 'js',
        link: 'https://www.equifyte.com',
    },
    {
        imgSrc: KhanhvuOne,
        client: 'Khanh Vu',
        tech: 'React',
        link: 'https://khanhv.vercel.app',
    },
    {
        imgSrc: BitmexOne,
        client: 'bitmex',
        tech: 'react, node, contentful',
        link: 'https://www.bitmex.com',
    },
    {
        imgSrc: NFTInternOne,
        client: 'ltltco',
        tech: 'nextjs, directus',
        link: 'https://nft-intern.vercel.app',
    },
];
