const Text = ({ children, className }) => {
  return (
    <span
      className={`font-body font-semibold leading-5 text-base tracking-tighter uppercase text-text-main ${className}`}
    >
      {children}
    </span>
  );
};

export default Text;
