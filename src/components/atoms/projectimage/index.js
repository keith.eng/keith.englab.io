const ProjectImage = ({ src }) => {
  return <img src={src} className="h-[70vh] rounded-2xl tablet:h-[80vh]" />;
};

export default ProjectImage;
