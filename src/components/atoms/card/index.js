const Card = ({ children, className }) => {
  return (
    <div className={`${className} w-full text-text-white rounded-2xl`}>
      {children}
    </div>
  );
};

export default Card;
