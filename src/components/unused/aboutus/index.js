import React, { useLayoutEffect } from 'react';
import Sunflower from '../../assets/sunflower.png';

const gsap = window.gsap;
const ScrollTrigger = window.ScrollTrigger;
gsap.registerPlugin(ScrollTrigger);

const AboutUs = React.forwardRef((props, ref) => {
  useLayoutEffect(() => {
    let matchMedia = gsap.matchMedia();

    const ctx = gsap.context((self) => {
      matchMedia.add('(min-width: 1024px)', () => {
        gsap.to('#image--sunflower', {
          yPercent: '+=100',
          scrollTrigger: {
            trigger: '#about-us__container',
            scrub: 1,
            end: 'top top',
          },
        });

        gsap.fromTo(
          '.about-us__text--color-gsap',
          {
            color: '#242726',
          },
          {
            ease: 'linear',
            color: '#d04b18',
            scrollTrigger: {
              trigger: '#about-us__container',
              start: 'top top',
              end: '+=30%',
              scrub: 0.5,
            },
          },
        );

        gsap.to('#image--sunflower', {
          ease: 'linear',
          rotation: 180,
          scrollTrigger: {
            trigger: '#about-us__container',
            start: 'top top',
            scrub: 0.5,
            pin: true,
          },
        });

        gsap.to('#image--sunflower', {
          yPercent: '+=100',
          immediateRender: false,
          scrollTrigger: {
            trigger: '#about-us__container',
            start: (self) => self.previous().end,
            end: '+=100%',
            scrub: 1,
          },
        });
      });
    }, ref);

    return () => ctx.revert();
  }, [ref]);

  return (
    <section ref={ref} id="aboutus">
      <div
        className="flex flex-col smallDesktop:flex-row justify-between items-center min-h-screen p-12 bg-background--light"
        id="about-us__container"
      >
        <span className="text-xl smallDesktop:self-end smallDesktop:w-1/3 bigDesktop:text-4xl tablet:text-3xl text-text">
          Designing{' '}
          <span className="about-us__text--color-gsap text-text--highlight-orange">
            mobile-first
          </span>{' '}
          interfaces with a focus on{' '}
          <span className="about-us__text--color-gsap text-text--highlight-orange">
            responsiveness
          </span>{' '}
          and{' '}
          <span className="about-us__text--color-gsap text-text--highlight-orange">
            accessibility
          </span>
          . Our approach prioritizes the{' '}
          <span className="about-us__text--color-gsap text-text--highlight-orange">
            user's experience
          </span>
          , ensuring a seamless transition on any devices.
        </span>
        <img
          src={Sunflower}
          id="image--sunflower"
          alt="Cartoon Sunflower smiling"
          className="smallDesktop:self-start tablet:h-[30vh] h-[20vh]"
        />
        <span className="text-xl smallDesktop:self-start smallDesktop:w-1/3 bigDesktop:text-4xl tablet:text-3xl text-text">
          We write{' '}
          <span className="about-us__text--color-gsap text-text--highlight-orange">
            adaptive web components
          </span>{' '}
          whilst reftaining{' '}
          <span className="about-us__text--color-gsap text-text--highlight-orange">
            optimization
          </span>{' '}
          for a better web surfing experience. Beyond that, our expertise on{' '}
          <span className="about-us__text--color-gsap text-text--highlight-orange">
            SEO
          </span>{' '}
          ensures that your website not only looks great, but also ranks high on
          search engines.
        </span>
      </div>
    </section>
  );
});

export default AboutUs;
