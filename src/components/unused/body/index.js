import HillTop from '../../assets/hill_top_transparent.png';

const BannerHalf = () => {
  return (
    <div className="max-h-screen overflow-hidden">
      <img
        src={HillTop}
        alt="Landscape of rocky hills and a lake in the middle"
      />
    </div>
  );
};

const BodyOne = () => {
  return (
    <div className="bg-[#fefdf8] min-h-screen flex flex-col justify-between p-16">
      <span className="text-end w-1/2 ml-auto">
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book.
      </span>
      <span className="text-center">Asshole</span>
      <span className="text-start w-1/2 mr-auto">
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book.
      </span>
    </div>
  );
};

const Body = () => {
  return (
    <div className="min-h-screen absolute z-1">
      <BannerHalf />
      <BodyOne />
    </div>
  );
};

export default Body;
