import React from 'react';

import HillTop from '../../assets/hill_top_transparent.png';

const LandingForeground = React.forwardRef((props, ref) => (
  <div className="max-h-screen overflow-hidden" ref={ref}>
    <img
      src={HillTop}
      alt="Landscape of rocky hills and a lake in the middle"
      className="h-screen w-screen object-cover mobile:invisible"
    />
  </div>
));

export default LandingForeground;
