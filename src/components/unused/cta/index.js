import Plains from '../../assets/plains.jpg';

const CTA = ({ contactScroll }) => {
  return (
    <a href="#contactus" onClick={contactScroll}>
      <div className="flex h-[30vh] items-center">
        <img
          src={Plains}
          alt="Landscape of rocky hills and a lake in the middle"
          className="h-[30vh] w-screen absolute object-cover border-y-2 border-black z-10"
        />
        <div className="z-20 w-screen text-center bg-white whitespace-nowrap border-y-2 border-black">
          <span className="text-5xl text-text--black uppercase">
            ecommerce business sales information digital ecommerce business
            sales information digital
          </span>
        </div>
      </div>
    </a>
  );
};

export default CTA;
