import { useState, useEffect } from 'react';

import About from '../templates/about';
import Projects from '../templates/projects';
import Information from '../templates/information';

import Text from '../atoms/text';

import { SmallDesktop } from '../responsive';

import Noise from '../../assets/noise.gif';

export const App = () => {
  const [isFirstLoad, setIsFirstLoad] = useState(true);

  const [isLoading, setIsLoading] = useState(true);
  const [isLanding, setIsLanding] = useState(true);

  const [isAboutOpen, setIsAboutOpen] = useState(false);
  const [isProjectsOpen, setIsProjectsOpen] = useState(false);
  const [isInformationOpen, setIsInformationOpen] = useState(false);

  const [windowWidthSize, setWindowWidthSize] = useState(window.innerWidth);

  useEffect(() => {
    setTimeout(
      () => {
        setIsLoading(false);
      },
      windowWidthSize >= 1024 ? 750 : 0,
    );
    setWindowWidthSize(windowWidthSize);
  }, [windowWidthSize]);

  const toggleLandingOff = () => {
    setIsLanding(false);

    setTimeout(
      () => {
        setIsProjectsOpen(true);
        setIsInformationOpen(true);
        setIsAboutOpen(true);
      },
      windowWidthSize >= 1024 ? 2000 : 0,
    );

    setTimeout(
      () => {
        setIsFirstLoad(false);
      },
      windowWidthSize >= 1024 ? 3000 : 0,
    );
  };

  return (
    <div
      className="App cursor-default h-full smallDesktop:h-screen w-screen overflow-hidden flex flex-col smallDesktop:flex-row text-text-main overflow-hidden"
      style={{
        backgroundImage: `linear-gradient(180deg, rgba(254, 249, 246, 1), rgba(254, 249, 246, 1)), url(${Noise})`,
      }}
      onClick={() => (!isLoading && isLanding ? toggleLandingOff() : undefined)}
    >
      <About isLanding={isLanding} />
      <SmallDesktop>
        <div
          className={`
          ${!isLoading && isLanding ? 'opacity-0' : 'opacity-1'}
          p-4 pt-32 
        `}
        >
          <span className="text-left text-3xl mb-16 smallDesktop:mb-32 flex justify-between text-text-red">
            information!
          </span>
          <div
            style={{
              fontSize: '0.8rem',
            }}
          >
            <div className="mb-24">
              <Text>
                I am a solo fullstack engineer. My focus is on developing
                accessible and responsive interfaces. Beyond that, I like to
                play indie games and am currently on a fervid pursuit for the
                mastery of the culinary arts.
              </Text>
            </div>
          </div>
        </div>
      </SmallDesktop>
      <div
        className={`
          flex flex-col smallDesktop:flex-row
          sections__container
          ${
            isLanding
              ? ''
              : isFirstLoad
              ? 'sections__container--anim-first-open'
              : isAboutOpen && !isProjectsOpen && !isInformationOpen
              ? 'sections__container--anim-close'
              : 'sections__container--anim-open'
          }
        `}
      >
        <Projects
          isProjectsOpen={isProjectsOpen}
          toggleProjects={() => setIsProjectsOpen(!isProjectsOpen)}
          windowWidthSize={windowWidthSize}
        />
        <Information
          isInformationOpen={isInformationOpen}
          toggleInformation={() => setIsInformationOpen(!isInformationOpen)}
          windowWidthSize={windowWidthSize}
        />
      </div>
    </div>
  );
};
