import Card from '../../atoms/card';

const Hero = () => {
  return (
    <section
      className="text-text-white text-center mb-32 break-words ml-auto mr-auto max-w-full"
      id="hero"
    >
      <h1 className="text-6xl mb-12 w-11/12 ml-auto mr-auto uppercase">
        Seamless user-centric design utilizing newest frontend technologies, all
        tailored for you.
      </h1>
      <Card className="h-[70vh] mb-32 bg-space-blood-orange" />
      <h2 className="text-5xl w-3/4 ml-auto mr-auto mb-2 uppercase">
        The complete package
      </h2>
      <h3 className="text-xl mobile:w-11/12 w-7/12 ml-auto mr-auto">
        Using my fullstack engineering expertise, be ensured all your business
        needs will be met while maintaining optimization. I also collaborate
        with an array of design specialists, each specializing in unique art
        styles to translate your vision into interactive & engaging websites.
      </h3>
    </section>
  );
};

export default Hero;
