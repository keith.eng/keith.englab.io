import Card from '../../atoms/card';

const AboutCard = ({ children, header, content, color }) => {
  return (
    <Card
      className={`flex flex-col p-1 sticky top-[30%] mb-2 h-[280px] ${color} rounded-t-sm`}
    >
      <h2 className="text-2xl capitalize px-6 py-4 mb-1 rounded-t-sm bg-bg-dark max-w-full break-words">
        {header}
      </h2>
      <p className="text-lg rounded-b-xl px-6 py-6 bg-bg-dark h-full max-w-full leading-6 flex items-start">
        {content}
      </p>
      {children}
    </Card>
  );
};

const About = () => {
  return (
    <section
      className="text-text-white flex flex-col smallDesktop:flex-row justify-between mx-4 bigDesktop:mx-[22rem] min-h-[70vh] mb-32"
      id="about"
    >
      <div className="smallDesktop:w-1/2 mb-4 mr-4 sticky top-[20%]">
        <div className="sticky top-[30%]">
          <h1 className="text-5xl">Scope</h1>
          <h3 className="text-2xl">What I do and why I do it</h3>
        </div>
      </div>
      <div className="smallDesktop:w-1/2">
        <AboutCard
          header="Responsive Responsive Responsive"
          content="Following mobile-first methodology, our designs allows all users regardless of devices have a seamless surfing experience."
          color="bg-space-orange"
        />
        <AboutCard
          header="Boosted for SEO"
          content="My approach to development prioritizes optimizing for search engines, ensuring that your website is easily discoverable by all potential leads."
          color="bg-bg-light"
        />
        <AboutCard
          header="Beyond 'up-to-date' tech integration"
          content="We stay ahead of the curve by incorporating AI and other modern technologies into our work. Fun fact, parts of this site is developed with the assistance of ChatGPT!"
          color="bg-space-orange"
        />
        <AboutCard
          header="Modular Architecture"
          content="Applying Atomic design methodology to our architecture results in a well-structured, modular and scalable components."
          color="bg-bg-light"
        />
        <AboutCard
          header="Look Good, Do Good, Feel Good"
          content="We value the power of collaboration and seek out unique designers to bring their diverse perspectives and creativity to each project."
          color="bg-space-orange"
        />
        <AboutCard
          header="Aftercare"
          content="Delivery is just the start of a site's lifespan. I am committed to building lasting partnerships and ensuring our product stays functional and up-to-date."
          color="bg-bg-light"
        />
      </div>
    </section>
  );
};

export default About;
