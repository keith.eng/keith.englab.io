import React, { useRef } from 'react';

const gsap = window.gsap;
const ScrollTrigger = window.ScrollTrigger;
gsap.registerPlugin(ScrollTrigger);

const Navbar = ({ landingScroll, aboutScroll, contactScroll, worksScroll }) => {
  const main = useRef();

  return (
    <div ref={main}>
      <nav
        className="w-full fixed flex justify-between z-40 px-4 py-2 text-md text-text-white"
        id="navbar__container"
      >
        <a href="#landing" onClick={landingScroll}>
          KE
        </a>
        <ul className="flex">
          <li className="navbar__item">
            <a href="#landing" onClick={landingScroll}>
              HOME
            </a>
          </li>
          <li className="navbar__item">
            <a href="#about" onClick={aboutScroll}>
              ABOUT
            </a>
          </li>
          <li className="navbar__item">
            <a href="#works" onClick={worksScroll}>
              WORKS
            </a>
          </li>
          <li className="navbar__item">
            <a href="#contactus" onClick={contactScroll}>
              CONTACT
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
