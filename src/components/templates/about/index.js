import Logo from '../../../assets/sealWithoutDither.png';
import Text from '../../atoms/text';

// Require change in SASS too
export const LogoLength = '150px';

const About = ({ isLanding }) => {
  return (
    <section
      id="About"
      className={`
        about
        ${isLanding ? 'about--full' : ''}
        h-screen items-end justify-center flex relative pb-0 flex-col smallDesktop:flex-row items-center smallDesktop:items-end
        anim__opacity--fill
      `}
    >
      <div
        className={`
          about__content
          opacity-0
          ${isLanding ? '' : 'anim__opacity--fill'}
          relative smallDesktop:absolute mr-auto left-0 p-4 h-full max-w-xs flex flex-col self-start order-last smallDesktop:order-1
        `}
      >
        <span className="text-5xl text-text-red">XUAN!</span>
        <Text className="mt-28 uppercase">
          the season after winter and before summer, in which vegetation begins
          to appear! the period from the vernal equinox to the summer solstice!
        </Text>
        <div className="mt-auto text-xs flex flex-col">
          <a
            href="https://gitlab.com/keith.eng"
            className="hover:underline"
            target="_blank"
            rel="noreferrer"
          >
            GitLab
          </a>
          <a
            href="https://www.linkedin.com/in/engkeith"
            className="hover:underline"
            target="_blank"
            rel="noreferrer"
          >
            LinkedIn
          </a>
          <a
            href="mailto:keith.eng@outlook.com"
            className="hover:underline"
            target="_blank"
            rel="noreferrer"
          >
            Contact: keith.eng@outlook.com
          </a>
        </div>
      </div>

      <img
        className="about__logo"
        src={Logo}
        alt="Chinese seal stamp of the word Spring in mandarin"
        height={LogoLength}
        width={LogoLength}
      />
      <span
        className={`
          about__text
          ${!isLanding ? 'anim__opacity--fade' : ''}
        `}
      >
        click to enter
      </span>
    </section>
  );
};

export default About;
