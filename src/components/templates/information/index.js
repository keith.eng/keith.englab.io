import { useState } from 'react';
import Text from '../../atoms/text';

const Information = ({
  isInformationOpen,
  toggleInformation,
  windowWidthSize,
}) => {
  const [toggled, setIsToggled] = useState(true);

  const toggleSection = () => {
    toggleInformation();
    setIsToggled(!toggled);
  };

  return (
    <section
      id="Information"
      className={`
        information
        ${
          isInformationOpen && toggled
            ? 'anim__grow--0-to-6'
            : 'anim__grow--6-to-0 cursor-pointer'
        }
        p-4 h-full items-center justify-center flex relative w-full smallDesktop:w-16 border-text-main border-l-0 smallDesktop:border-l-2
      `}
      onClick={() => (isInformationOpen ? undefined : toggleSection())}
    >
      <span
        className={`
          information__title
          ${
            isInformationOpen
              ? 'information__title--opened'
              : 'information__title--closed'
          }
        `}
      >
        information
      </span>
      <div
        className={`
          information__content
          ${
            isInformationOpen
              ? 'information__content--opened-anim'
              : 'information__content--closed-anim'
          }
          flex flex-col h-full overflow-hidden tablet:overflow-y-scroll pr-4
        `}
      >
        <button
          onClick={() => (windowWidthSize < 1024 ? undefined : toggleSection())}
          className="text-left text-3xl mb-24 smallDesktop:mb-32 flex justify-between text-text-red hidden smallDesktop:flex"
        >
          <span>information!</span>
          <span className="hidden smallDesktop:block">x</span>
        </button>
        <div
          className="mb-16 smallDesktop:mb-0"
          style={{
            fontSize: '0.8rem',
          }}
        >
          <div className="mb-24 hidden smallDesktop:block">
            <Text>
              I am a solo fullstack engineer. My focus is on developing
              accessible and responsive interfaces. Beyond that, I like to play
              indie games and am currently on a fervid pursuit for the mastery
              of the culinary arts.
            </Text>
          </div>
          <div className="flex flex-col tablet:flex-row justify-between">
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
              }}
              className="mr-16 tablet:mr-32 my-8 tablet:my-0"
            >
              <span className="text-3xl tablet:text-2xl text-text-red mb-2 tablet:mb-0">
                Focuses!
              </span>
              <br />
              <hr
                className="hidden tablet:block"
                style={{
                  border: '0.1px solid',
                }}
              />
              <br />
              <br />

              <Text>
                Following{' '}
                <span className="text-text-red">mobile-first methodology</span>,
                we ensure our designs allow all users regardless of devices have
                a seamless surfing experience.
              </Text>
              <br />
              <br />

              <Text>
                Our approach to development prioritizes{' '}
                <span className="text-text-red">
                  optimizing for search engines
                </span>
                , guaranteeing discoverability by all potential leads.
              </Text>
              <br />
              <br />

              <Text>
                <span className="text-text-red">incorporating AI</span> and
                other technologies into our work. Fun fact, parts of this site
                is developed with the assistance of ChatGPT!
              </Text>
              <br />
              <br />

              <Text>
                Applying modern design methodologies to our architecture results
                in a structured, <span className="text-text-red">scalable</span>{' '}
                and maintainable solution
              </Text>
              <br />
              <br />
            </div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
              }}
              className="mr-16 tablet:mr-0"
            >
              <span className="text-3xl tablet:text-2xl text-text-red my-16 mb-2 tablet:my-0">
                Technologies!
              </span>
              <br />
              <hr
                className="hidden tablet:block"
                style={{
                  border: '0.1px solid',
                }}
              />
              <br />
              <br />

              <Text>ReactJS</Text>
              <Text>NextJS</Text>
              <Text>Typescript</Text>
              <br />
              <br />

              <Text>Atomic Design</Text>
              <Text>Storybook</Text>
              <br />
              <br />

              <Text>NodeJS</Text>
              <Text>GraphQL</Text>
              <Text>Contentful</Text>
              <Text>DirectUs</Text>
              <br />
              <br />

              <Text>DataDog</Text>
              <Text>Google Analytics</Text>
              <Text>Sentry</Text>
              <Text>TestRail</Text>
              <br />
              <br />

              <Text>MongoDB</Text>
              <Text>MySQL</Text>
              <Text>Heroku</Text>
              <Text>Vercel</Text>
              <br />
              <br />

              <Text>Python</Text>
              <Text>Java</Text>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Information;
