import { useState } from 'react';

import Project from '../../molecules/project';
import { ProjectList } from '../../../constants/constants';

const Projects = ({ isProjectsOpen, toggleProjects, windowWidthSize }) => {
  const [toggled, setIsToggled] = useState(true);

  const toggleSection = () => {
    toggleProjects();
    setIsToggled(!toggled);
  };

  return (
    <section
      id="Projects"
      className={`
        projects
        ${
          isProjectsOpen && toggled
            ? 'anim__grow--0-to-4'
            : 'anim__grow--4-to-0 cursor-pointer'
        }
        p-4 smallDesktop:pb-4 h-full items-center justify-center flex relative w-full smallDesktop:w-16 border-text-main border-l-0 smallDesktop:border-l-2
      `}
      onClick={() => (isProjectsOpen ? undefined : toggleSection())}
    >
      <span
        className={`
          projects__title
          ${
            isProjectsOpen
              ? 'projects__title--opened'
              : 'projects__title--closed'
          }
        `}
      >
        projects
      </span>
      <div
        className={`
          projects__content
          ${
            isProjectsOpen
              ? 'projects__content--opened-anim'
              : 'projects__content--closed-anim'
          }
          flex flex-col h-full overflow-y-scroll pr-6
        `}
      >
        <button
          onClick={() => (windowWidthSize < 1024 ? undefined : toggleSection())}
          className="text-left text-3xl mb-12 smallDesktop:mb-32 flex justify-between text-text-red"
        >
          <span>projects!</span>
          <span className="hidden smallDesktop:block">x</span>
        </button>
        {ProjectList &&
          ProjectList.map((project) => {
            return (
              <Project {...project} key={project.client} clickable={toggled} />
            );
          })}
      </div>
    </section>
  );
};

export default Projects;
