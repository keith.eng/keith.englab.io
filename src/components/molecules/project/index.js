import Text from '../../atoms/text';

const Project = ({ imgSrc, client, tech, link, clickable }) => {
  const projectContent = (
    <div
      className="mb-24"
      style={{
        position: 'relative',
      }}
    >
      <img
        src={imgSrc}
        alt={`A screenshot to showcase the website of the ${client}`}
        style={{
          border: '2px solid black',
        }}
      />
      <div className="flex flex-row justify-between mt-1">
        <div className="flex flex-col w-1/2">
          <Text className="">client</Text>
          <Text className="">{client}</Text>
        </div>
        <div className="flex flex-col w-1/2">
          <Text className="">tech</Text>
          <Text className="">{tech}</Text>
        </div>
      </div>
    </div>
  );
  return (
    <>
      {clickable ? (
        <a href={link} target="_blank" rel="noreferrer">
          {projectContent}
        </a>
      ) : (
        <div>{projectContent}</div>
      )}
    </>
  );
};

export default Project;
