import React from 'react';
import { useMediaQuery } from 'react-responsive';

export const SmallDesktop = (props) => {
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1023px)' });

  return <>{isTabletOrMobile ? props.children : <></>}</>;
};
